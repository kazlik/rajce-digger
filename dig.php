<?php
/**
 * Created by PhpStorm.
 * User: Marek
 * Date: 02.12.2018
 * Time: 18:02
 */

if(empty($argv[1])) {
	echo "Specify rajce url for digging!";
	die();
}

$path = $argv[1];
$explodedPath = explode('/', $path);
$folder = __dir__ . '/photos/' . $explodedPath[2] . '/' . $explodedPath[3] . '/';
echo "Downloading photogallery...";
$content = file_get_contents($path);
echo "\tdone!";
$images = [];

preg_match_all('/\"fileName\":\"([^\"]+)\"/m', $content, $matches, PREG_SET_ORDER, 0);
preg_match_all('/var storage = \"([^\"]+)\"/', $content, $urlMatches, PREG_SET_ORDER, 0);
$url = str_replace('\\', '', $urlMatches[0][1]);

foreach ($matches as $match) {
	if(isset($match[1])) {
		$images[] = $url . "images/" . $match[1];
	}
}

if (!file_exists($folder)) {
	mkdir($folder, 0777, true);
}

foreach ($images as $image) {
	$imageName = basename($image);
	$imagePath = $folder . $imageName;
	echo "\nDownloading image " . $imageName . "...";
	$imageContent = file_get_contents($image);
	file_put_contents($imagePath, $imageContent);
	echo "\tdownloaded!";
}

echo "\nPhotogallery downloaded successfully";